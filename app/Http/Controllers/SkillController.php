<?php

namespace App\Http\Controllers;

use App\Models\Skill;
use Illuminate\Http\Request;

class SkillController extends Controller
{
    public function get()
    {
        try {
            $skills = Skill::select('id', 'name')->get();

            return response()->json([
                'success'   => true,
                'data'      => $skills
            ], 201);
        } catch (\Exception $e) {
            return response()->json([
                'success'   => false,
                'data'      => $e->getMessage()
            ], 201);
        }
    }
}
