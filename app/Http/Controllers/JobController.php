<?php

namespace App\Http\Controllers;

use App\Models\Job;
use Illuminate\Http\Request;

class JobController extends Controller
{
    public function get()
    {
        try {
            $jobs = Job::select('id', 'name')->get();

            return response()->json([
                'success'   => true,
                'data'      => $jobs
            ], 201);
        } catch (\Exception $e) {
            return response()->json([
                'success'   => false,
                'data'      => $e->getMessage()
            ], 201);
        }
    }
}
