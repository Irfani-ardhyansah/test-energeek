<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class JobSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'name' => "Frontend Web Programmer",
            ],            
            [
                'name' => "Fullstack Web Programmer",
            ],            
            [
                'name' => "Quality Control",
            ]
        ];

        \DB::table('jobs')->insert($data);
    }
}
