<?php

use App\Http\Controllers\JobController;
use App\Http\Controllers\SkillController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\StoreController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

// endpoint to get skill data
Route::get('/skill', [SkillController::class, 'get']);

// endpoint to get job data
Route::get('/job', [JobController::class, 'get']);

// endpoint to store candidate data
Route::post('/candidate/store', [StoreController::class, 'store']);
