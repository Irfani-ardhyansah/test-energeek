<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\Models\Candidate;
use App\Models\SkillSet;

class StoreController extends Controller
{
    public function store(Request $request)
    {
        // db transaction
        DB::beginTransaction();

        try {
            // validation
            $validator = Validator::make($request->all(), [
                'job'           => 'required', 
                'phone'         => 'required|numeric|unique:candidates',
                'email'         => 'required|email|unique:candidates',
                'tahun_lahir'   => 'required',
                'skill_set'     => 'required'
            ]);

            // if validation fail
            if($validator->fails()) {
                DB::rollBack();

                return response()->json([
                    'success'   => false,
                    'data'      => $validator->errors()->first()
                ], 201);
            }

            // if have redundant data
            if(count(array_unique($request->skill_set)) < count($request->skill_set)){
                DB::rollBack();

                return response()->json([
                    'success'   => false,
                    'data'      => 'Skill Set Have Redundant Data'
                ], 201);
            }

            // function to store data candidate
            // job input with job_id foreign key
            $candidate = Candidate::create([
                'job_id'    => $request->job,
                'phone'     => $request->phone,
                'email'     => $request->email,
            ]);

            // loop for skill_set
            foreach($request->skill_set as $row) {
                // function to store data skill set
                // candidate_id and skill_id input using foreign key
                SkillSet::create([
                    'candidate_id' => $candidate->id,
                    'skill_id'     => $row
                ]);
            }
            // if success
            DB::commit();
            return response()->json([
                'success'   => true,
                'data'      => $candidate
            ], 201);
        } catch(\Exception $e) {
            // if fails
            DB::rollBack();
            return response()->json([
                'success'   => true,
                'data'      => $e->getMessage()
            ], 500);
        }
    }
}
