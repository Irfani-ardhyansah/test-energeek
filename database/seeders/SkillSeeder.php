<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class SkillSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'name' => "PHP",
            ],            
            [
                'name' => "PostgreSQL",
            ],            
            [
                'name' => "API(JSON, REST)",
            ]
        ];

        \DB::table('skills')->insert($data);
    }
}
