<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSkillSetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('skill_sets', function (Blueprint $table) {
            $table->unsignedBigInteger('candidate_id')->nullable();
            $table->unsignedBigInteger('skill_id')->nullable();

            $table->unsignedBigInteger('candidate_id')->nullable()->unsigned()->change();
            $table->foreign('candidate_id')->references('id')->on('candidates')->onDelete('cascade');
            $table->unsignedBigInteger('skill_id')->nullable()->unsigned()->change();
            $table->foreign('skill_id')->references('id')->on('skills')->onDelete('cascade');
        
            $table->primary(['candidate_id','skill_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('skill_sets');
    }
}
